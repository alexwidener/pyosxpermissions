pyOSXPermissions
================
A quick script to change permissions on all files in a directory and subdirectories to "open" on OSX. 

This was written so I could unlock a ton of .mp3 files that I purchased from HumbleBundle (http://www.humblebundle.com) and through Steam, but when I unzipped the files, I didn't have permissions to the files and could not use the TAGR app on OS X to edit their ID3vx? tags. (? = I don't know the version). The TAGR app was telling me the permissions were not correctly set on files, and there are a lot of files so I built this little script to run through a directory that I select and unlock all .mp3 files.

Change your extension to the file extension you would like to use and if you'd like to change the permissions, on the subprocess.call line, edit 777(full access) to something else.

700 = User only
You can look up the rest on Google. 

Don't do this on anything other than pictures and audio files. I have no idea what that could do. 
