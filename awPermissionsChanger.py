#!/user/bin/env python

# Run, select a folder, and it will change all permissions in subsequent folders and files to be open
# Beware, this could be destructive if you run this at a very low-level folder structure... so don't do that.
# Please see the read and license.
# I assume no responsibility for your use of this in any way. If you wreck something, it is your fault for using it.
# The only reason I created this was to use it on mp3 files so I could mass unlock them and run another program on them to edit the id3v2 tags.

__author__ = "Alex Widener"
__copyright__ = "Copyright 2014, Alex Widener"
__license__ = "GNU GPL 3.0"
__version__ = "0.01"
__email__ = "alexwidener # gmail"
__status__ = "Development"

import Tkinter, Tkconstants, tkFileDialog, os, subprocess

class awPermissionsChanger(Tkinter.Frame):
    def __init__(self, root):
        Tkinter.Frame.__init__(self, root)
        button_opt = {'fill': Tkconstants.BOTH, 'padx' : 5, 'pady' : 5}
        Tkinter.Button(self, text='NavigateToDirectory', command=self.askdirectory).pack(**button_opt)
        self.dir_opt = {}
        self.workingDirectory = ''
        self.fileList = []

    def askdirectory(self):

        self.workingDirectory = tkFileDialog.askdirectory(**self.dir_opt)
        print 'workDirectory is ' + self.workingDirectory
        self.readDirectory()

    def readDirectory(self):

        for root, dirs, files in os.walk(self.workingDirectory):
            for file in files:
                if file.endswith('.mp3'):
                    fPath = os.path.join(root, file)
                    self.fileList.append(fPath)
        self.changePermissions()

    def changePermissions(self):
        for everyFile in self.fileList:
            print everyFile
            subprocess.call(["ls", "-l"])
            subprocess.call(["chmod", "777", everyFile])
if __name__ == '__main__':
    root = Tkinter.Tk()
    awPermissionsChanger(root).pack()
    root.mainloop()